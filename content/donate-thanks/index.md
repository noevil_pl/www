---
title: "Dziękujemy!"
---

## Dziękujemy za wsparcie!
Darowizny pozwalają utrzymać nasze darmowe usługi. Twoje zaangażowanie w budowę wolnych i otwartych alternatyw w Internecie wiele dla nas znaczy.

<center><img onclick="confetti();" src="grateful_monkey.png" alt="" align="center" height="300"/></center>

<script src="confetti.js"></script>
<script>confetti();</script>
