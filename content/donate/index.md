---
title: "Wsparcie"
menu:
  main:
    pre: 🚀
    weight: 3
---

## Wsparcie
Przyjmujemy dobrowolne wpłaty na pokrycie kosztów naszych usług.

<center><img src="monkey_donate.png" alt="" title="❤️"/></center>

Serwer [Matrix](/matrix) działa dzięki uprzejmości Fundacji Technologie dla Ludzi, najlepszym sposobem jego wsparcia są wpłaty bezpośrednie - [link](https://ftdl.pl/donate/).
{.notice}

## Przelew bankowy

Numer konta (IBAN)
:   PL 11 1020 4476 0000 8502 0427 2027

Nazwa odbiorcy
:   Dawid Rejowski

Tytuł
:   Na noevil.pl
