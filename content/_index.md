---
title: "Strona Główna"
layout: single
menu:
  main:
    weight: 1
---

Dołącz do nas i poznaj rozwiązania przyjazne prywatności!
Nasze usługi są w 100% oparte o [wolne oprogramowanie](https://pl.wikipedia.org/wiki/Wolne_oprogramowanie) i nie zawierają reklam.

## Hostujemy
{{< index-service "Matrix" "/matrix" "Otwarta sieć dla <span class=\"spoiler\">szyfrowanej</span> i zdecentralizowanej komunikacji." >}}

## Podziękowania
<a href="https://ftdl.pl"><img src="ftdl.webp" width="200px"></a>
