---
title: "[Matrix]"
layout: single
---

## Czym jest <picture><source srcset="matrix-logo_darkmode.svg" media="(prefers-color-scheme: dark)"><img src="matrix-logo.svg" alt="Matrix" style="vertical-align:middle; width:120px; border-radius:0"></picture>?

[Matrix](https://matrix.org) to ambitny projekt stworzenia otwartego standardu dzięki któremu możliwe jest tworzenie kompatybilnych ze sobą aplikacji do chatów i rozmów głosowych.

<center><img src="monkey.png" alt="" title="Wiadomość, szefie!"/></center>

Możesz wysłać wiadomość email do każdego, nawet jeśli ma pocztę u innego dostawcy, zadzwonić do osoby u każdego operatora, dlaczego internetowe komunikatory nie miałyby działać podobnie?

<figure style="width: 50%; display: inline-block;"><img src="example-centralized.png"/><figcaption>🚫 W zamkniętych i zcentralizowanych serwisach małpki nie mogą swobonie rozmawiać ze sobą, muszą instalować wiele śledzących je aplikacji, akceptując rozrosłe regulaminy.</figcaption></figure>
<figure style="width: 50%; display: inline-block;"><img src="example-decentralized.png"/><figcaption>✅ W otwartej sieci (takiej jak Matrix, Email czy SMS) małpki rozmawiają niezależnie od tego u jakiego dostawcy mają, konto używając swoich ulubionych aplikacji.</figcaption></figure>

## Jak dołączyć?

Kliknij przycisk poniżej, aby założyć konto w przeglądarce.\
Możesz też użyć aplikacji obsługującej Matrix. My polecamy aplikację [Element](https://element.io/download), która jest prosta w użyciu i dostępna na każde urządzenie.

<a class="button hover-finger" href="https://chat.noevil.pl/#/register">Utwórz konto</a>

<details>
<summary>Rejestracja i logowanie w aplikacji</summary>
<p>
    Uruchom dowolną aplikację (mobilną, na komputer lub w przeglądarce) obsługującą Matrix i <b>ustaw serwer jako noevil.pl przy rejestracji lub logowaniu.</b>
</p>
<p>
    <center>
    <figure style="width: 50%; display: inline-block;">{{< video-mp4 file="element-android.mp4" width="75%" >}}<figcaption>Przykład dla Element Android</figcaption></figure>
    <figure style="width: 50%; display: inline-block;">{{< video-mp4 file="fluffychat.mp4" width="75%" >}}<figcaption>Przykład dla FluffyChat</figcaption></figure>
    <figure>{{< video-mp4 file="element-desktop.mp4" width="100%" >}}<figcaption>Przykład dla Element Desktop</figcaption></figure>
    </center>
</p>
</details>

## Pomoc

Jeśli potrzebujesz pomocy, dołącz do [naszego pokoju](https://matrix.to/#/#cafe:noevil.pl), możesz też napisać do administratora przez kontakt w stopce.

## Szyfrowanie E2E

<picture>
    <source srcset="shield_darkmode.png" media="(prefers-color-scheme: dark)"> <img src="shield.png" alt="Ikona tarczy">
</picture> oznacza, że pokój jest szyfrowany. Nikt poza jego uczestnikami nie jest w stanie zobaczyć wiadomości.

![Zielona ikona tarczy](shield-verified.png) oznacza, że pokój jest szyfrowany oraz urządzenia zostały zweryfikowane. Nikt poza uczestnikami nie jest w stanie zobaczyć wiadomości oraz masz pewność, że nikt nie podszywa się pod rozmówców.

<details>
<summary>Jak włączyć szyfrowanie pokoju?</summary>
<p>Wejdź w jego ustawienia i kliknij "Włącz szyfrowanie end-to-end".</p>
</details>

<details>
<summary>Jak zweryfikować urządzenia uczestników (uzyskać zieloną tarczkę)?</summary>
<p>Spotkajcie się fizycznie. Wejdź w ustawienia któregoś z pokoi, natępnie w listę osób i wybierz osobę. Na jej profilu kliknij "Zweryfikuj".
Zostaniecie poproszeni o zeskanowanie kodów QR lub sprawdzenie kodu emoji.</p>
</details>

<details>
<summary>Kto ma dostęp do kluczy w pokojach?</summary>
<p>Klucze do zaszyfrowanych pokoi dostępne są jedynie na urządzeniach uczestników. Dlatego gdy logujesz się na nowe urządzenie, zostaniesz poproszony o zeskanowanie kodu QR z poprzedniego, aby mogły się bezpiecznie przenieść.</p>
</details>

<details>
<summary>Co jeśli wszystkie urządzenia mam wylogowane, albo nie mam do nich dostępu?</summary>
<p>Wtedy należy wpisać hasło kopii zapasowej.
Jeśli go już nie masz lub nie zostało ustawione, zweyfikuj urządzenie ze swoimi rozmówcami (tak samo jak uzyskuje się zieloną tarczkę).</p>
</details>

<details>
<summary>Do czego służy hasło kopii zapasowej?</summary>
<p>Dzięki twojemu hasłu głównemu możesz zalogować się na nasz serwer. Nie mamy za to dostępu do kluczy w pokojach, a więc i twoich szyfrowanych wiadomości.
Klucze możesz przenieść z urządzenia na urządzenie (np. gdy masz nowy telefon), po zalogowaniu zostaniesz poproszony o zeskanowanie kodu QR ze starego urządzenia.
Drugie hasło (kopii zapasowej) służy jako alternatywa i może ci się przydać, jeśli przypadkiem wylogujesz się ze wszystkich urządzeń lub nie będziesz mieć do nich dostępu.</p>
</details>

## Usunięcie konta

Opcję skasowania konta znajdziesz w ustawieniach w aplikacji.

<details>
<summary>Dla Element</summary>

- Zaloguj się
- Kliknij na swoje zdjęcie profilowe
- Wejdź w ustawienia ogólne
- Tam znajdziesz opcję "Dezaktywuj moje konto"
</details>

<details>
<summary>Dla FluffyChat</summary>

- Zaloguj się
- Kliknij Swoje zdjęcie profilowe
- Wejdź w "Ustawienia"
- Wejdź w opcję "Konto"
- Wybierz opcję "Usuń konto"
</details>

Możesz też poprosić administratora przez kontakt na stronie.
