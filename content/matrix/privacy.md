---
title: "Polityka Prywatności serwera Matrix"
---

## 1. Zakres dokumentu
Niniejszy dokument dotyczy jedynie oferowanego przez noevil.pl serwera sieci Matrix i ma na celu przybliżenie zasad jakimi kierujemy się przetwarzając dane. Korzystanie z usługi wymaga jego akceptacji oraz akceptacji [ogónej polityki prywatności noevil.pl](/privacy-policy).

## 2. Strony trzecie
Możesz używać klienta, który korzysta z usług nieobsługiwanych przez noevil.pl, takich jak: 
- Wideokonferencje Jitsi, z funkcji połączeń video lub z menedżera integracji (zwykle Element.io)
- Managera Integracji (zwykle hostowanego przez Matrix.org)
- Serwera tożsamości (Matrix.org)
- Serwera kluczy (Matrix.org)

## 3. Mostki
Niektóre pokoje używają mostków, aby połączyć się z sieciami innymi niż Matrix, takimi jak IRC, Discord lub Telegram. Kiedy pokój korzysta z takiego bota lub aplikacji, wiadomości i media kopiowane są do połączonych sieci. noevil.pl nie jest odpowiedzialny ani nie jest w stanie zagwarantować kontroli nad danymi udostępnianymi do tych platform.

## 4. Kto może zobaczyć twoje wiadomości?
1) W nieszyfrowanych pokojach, użytkownicy mogą odczytywać treść wiadomości i przesłanych plików zgodnie z nadanymi prawami do takiego pokoju. Te dane mogą być odczytane przez wszystkich użytkowników w pokoju oraz administratorów noevil.pl

2) W szyfrowanych pokojach (w aplikacji Element oznaczonych ikoną tarczy), dane są przechowywane na naszych serwerach, ale mogą być odczytane jedynie używając kluczy przechowywanych w programach klienckich. Treść wiadomości zostaje dostępna jedynie dla uczestników rozmowy, żaden z administratorów noevil.pl ani dostawców hostingu nie jest w stanie ich odczytać.

## 5. Zmiany w dokumencie
Niniejszy dokument może być zmieniany z biegiem czasu, a jego historia dostępna jest do publicznego wglądu poprzez oprogramowanie [Git](https://pl.wikipedia.org/wiki/Git_(oprogramowanie)).

*[Historia dokumentu](https://codeberg.org/didek/noevil-www/commits/branch/main/content/privacy-matrix.md)*

*Ostatnia modyfikacja: {{< terms-lastmod >}}*
