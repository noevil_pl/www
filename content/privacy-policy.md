---
title: "Polityka Prywatności"
---

## Notka o języku
Niniejszy dokument ma być prosty i przystępny, aby mógł być prawidłowo zrozumiany przez użytkowników tego serwisu, którzy zazwyczaj nie są prawnikami. Nie jest on sporządzony przez ani dla prawników, a jego przyjęcie odbywa się na własne ryzyko.

## 1. Zakres dokumentu
Niniejszy dokument dotyczy wszystkich usług oferowanych przez noevil.pl i ma na celu przybliżenie zasad, jakimi kierujemy się przetwarzając dane.

## 2. Dokumenty dodatkowe
Korzystanie z poszczególnych usług może wymagać zaakceptowania dodatkowych dokumentów.

[Polityka prywatności serwera sieci Matrix](/matrix/privacy)

## 3. Lokalizacja serwisu
noevil.pl jest serwisem z Polski, a jego serwery znajdują się u wymienionych dostawców:
- [1984 ehf.](https://1984.hosting/) (Islandia)
- [Fundacja Technologie dla Ludzi](https://ftdl.pl/) (Polska)

Dodatkowo noevil.pl przechowuje kopie zapasowe danych w usłudze BorgBase firmy Peakford Ltd (Unia Europejska) w formie zaszyfrowanej i niedostępnej do wglądu przez Peakford Ltd.

## 4. Federalizacja
Możesz uczestniczyć w wymianie informacji (np. w pokojach w sieci Matrix) z użytkownikami innych serwerów. Nie możemy kontrolować w sposób pewny, jak ci dostawcy przetwarzają wiadomości i treści przesyłane przez ciebie poprzez federalizację.

## 5. Dane, które zbieramy
Oprogramowanie znajdujące się na serwerach może zbierać dane w celu moderacji, debugowania oraz zapobiegania publikacji spamu.
Obejmuje to między innymi:
- Twój adres IP
- Twój http-referrer (strona, z której przychodzisz)
- Używane oprogramowanie, jak wersja przeglądarki lub systemu operacyjnego
- Kiedy byłeś w ostanim czasie najbardziej aktywny

## 6. Usunięcie danych
Możesz w każdej chwili usunąć swoje dane z naszych serwerów poprzez użycie funkcji wbudowanej w klienty usług lub kontakt z nami.
Jeśli zdecydujesz się na usunięcie wiadomości lub wpisu, użytkownicy na naszym serwerze nie będą mogli przeczytać jej treści. Nie możemy jednak zagwarantować, że żądanie to będzie honorowane przez zdalne serwery i usługi.

## 7. Eksport i edycja danych
Możesz pobrać lub edytować dane przechowywane na naszych serwerach. W zależności od używanego klienta, możesz być w stanie wyeksportować potrzebne informacje bez konieczności kontaktowania się z nami.

## 8. Ochrona prywatności nieletnich
Nie zbieramy i nie przechowujemy informacji od osób, o których wiemy, że są poniżej 16 roku życia. Żadna część serwisu nie jest przeznaczona dla osób poniżej 16 roku życia. Jeśli masz mniej niż 16 lat, nie wolno Ci korzystać z serwisu.

## 9. Zmiany w tym dokumencie
Ten dokument może być aktualizowany. Historia zmian jest publiczna i dostępna do wglądu dzięki oprogramowaniu [Git](https://pl.wikipedia.org/wiki/Git_(oprogramowanie)).

*[Historia dokumentu](https://codeberg.org/didek/noevil-www/commits/branch/main/content/privacy-policy.md)*

*Ostatnia modyfikacja: {{< terms-lastmod >}}*
