---
title: "Regulamin"
---

Nieprzestrzeganie regulaminu będzie skutkowało blokadą korzystania z serwisu.
Oto kilka rzeczy, na które nie pozwalamy:

- treści nielegalne na terenie Polski lub kraju użytkownika
- celowe niszczenie kont innych użytkowników
- celowe zakłócanie pracy serwerów noevil.pl
- rozpowszechnianie pornografii każdego gatunku i medium (zwłaszcza pornografii dziecięcej)
- rozpowszechnianie treści zniesławiających jakąkolwiek osobę
- spamowanie na wszelkich forach, listach dyskusyjnych, kanałach irc/matrix, blogach i innych serwisach

Użytkownicy, którzy naruszą warunki użytkowania, zostaną nieodwołalnie usunięci bez powiadomienia.

Jeśli natkniesz się na coś, co narusza warunki korzystania z serwisu, poinformuj nas przez zakładkę "kontakt" znajdującą się na stronie głównej lub poprzez funkcję wbudowaną w aplikację.

*Ostatnia modyfikacja: {{< terms-lastmod >}}*
